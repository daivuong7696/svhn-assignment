Bước 1: chuyển dataset qua tfrecord
Nhớ chỉnh lại --dataset_dir thành đường dẫn tới file train_32x32.mat và test_32x32.mat

Chạy script:
python3 download_and_convert_data.py \
--dataset_name=svhn \
--dataset_dir=/home/daivuong/svhn-assignment/dataset/


Bước 2: train
Chỉnh lại:
--data_dir thành đường dẫn tới file dataset (như --dataset_dir ở script trên)
--model_dir folder chứa checkpoint

Các thiết lập mặc định (nếu muốn chỉnh thì có thể set thêm):
--resnet_size=50
--train_epochs=250
--epochs_per_eval=5 sau 5 epoch thì eval 1 lần, eval này là eval trên tập test luôn, chứ ko tạo riêng tập eval
--batch_size=128

Thông tin thêm:
Lưu checkpoint: sau mỗi lần eval nó sẽ tự lưu lại checkpoint, tuy nhiên sau mỗi 3600 giây nó cũng tự lưu lại checkpoint.
Thời gian lưu lại checkpoint thì chỉnh trong file svhn_main.py dòng 249
Decay learning_rate: giảm learning rate mỗi khi chạy xong 10 epoch, 50 epoch và 100 epoch có thể sửa lại
nhưng nên để y cũ để đồng bộ với 2 model t đã train trước đó.

Chạy script: 
python3 svhn_main.py \
--data_dir=/home/daivuong/svhn-assignment/dataset/ \ 
--model_dir=/home/daivuong/svhn-assignment/Resnet/model-50
