import scipy.io
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from sklearn import svm, neighbors, metrics
import datetime as dt
from skimage.feature import hog
from skimage.color import rgb2gray
import pickle
import math
import itertools
import matplotlib.pyplot as plt
train_set = scipy.io.loadmat('train_32x32.mat')
test_set = scipy.io.loadmat('test_32x32.mat')

def rbgTogray(train_images):
	leng = len(train_images[0,0,0,:])
	gray = []
	for i in range(leng):
		a =  rgb2gray(train_images[:,:,:,i])[2:30,2:30]
		gray.append(a)
	return np.array(gray)

def HOG_descriptor(train_images, norm):
	result = []
	leng = len(train_images[:,0,0])
	for i in range (leng):
		hog_image = hog(train_images[i,:,:], orientations=8, pixels_per_cell=(4,4),
                    cells_per_block=(4,4), visualise=True, block_norm=norm)[1].reshape(28*28)
		result.append(hog_image)
	return np.array(result)

def preprocess(image, min_divisor=1e-4):

	imsize = image.shape[0]

	mean = np.mean(image, axis=(1,2), dtype=float)
	std = np.std(image, axis=(1,2), dtype=float, ddof=1)
	
	std[std < min_divisor] = 1
	image_norm = np.zeros(image.shape, dtype=float)

	for i in np.arange(imsize):
  		image_norm[i,:,:] = (image[i,:,:] - mean[i]) / std[i]

	return image_norm

		 
train_img = train_set['X']
train_img = rbgTogray(train_img)
train_img = preprocess(train_img)
train_img = HOG_descriptor(train_img, i)
train_labels = np.array(train_set['y']).reshape(len(train_set['y']),) # 73257

test_img  = test_set['X']
test_img = rbgTogray(test_img)
test_img = preprocess(test_img)
test_img = HOG_descriptor(test_img, i)
test_labels = np.array(test_set['y']).reshape(len(test_set['y']),) 

X_train, y_train = train_img, train_labels
X_test, y_test = test_img, test_labels

clf = RandomForestClassifier(n_estimators=1000, n_jobs=10, max_features=30)
clf.fit(X_train,y_train)
acc = clf.score(X_test,y_test)

print('HOG + RF Score: ',acc)

	