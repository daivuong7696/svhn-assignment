import pandas as pd
from sklearn.decomposition import PCA
from skimage.filters import threshold_minimum, threshold_local
from sklearn import svm
import pickle
import scipy.io
from math import sqrt
from skimage.color import rgb2gray
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

train_set = scipy.io.loadmat('train_32x32.mat')
test_set = scipy.io.loadmat('test_32x32.mat')

def rbgTogray(images):
	leng = len(images[0,0,0,:])
	gray = []
	for i in range(leng):
		a =  rgb2gray(images[:,:,:,i])
		gray.append(a)
	return np.array(gray)

def normalization(images, c=10, min_divisor=1e-4):
	imsize = images.shape[0]
	mean = np.mean(images, axis=(1,2), dtype=float)
	std = np.std(images, axis=(1,2), dtype=float, ddof=1)
	std[std < min_divisor] = 1
	images_norm = np.zeros(images.shape, dtype=float)
	for i in np.arange(imsize):
  		images_norm[i,:,:] = (images[i,:,:] - mean[i]) / np.sqrt(std[i] + c)
  		

	return images_norm.reshape((len(images_norm),32*32))


train_img = train_set['X']
train_img = rbgTogray(train_img)
train_img = normalization(train_img)

train_labels = np.array(train_set['y']).reshape(len(train_set['y']),)

test_img = test_set['X']
test_img = rbgTogray(test_img)
test_img = normalization(test_img)
test_labels = np.array(test_set['y']).reshape(len(test_set['y']),)

X_train, X_valid, y_train, y_valid  = train_test_split(train_img, train_labels, test_size=0.1)
X_test, y_test = test_img, test_labels

print(X_train.shape, X_valid.shape, y_train.shape, y_valid.shape)
pca = PCA(n_components=0.9,whiten=True)
X_train = pca.fit_transform(X_train)
X_test = pca.transform(X_test)
X_valid = pca.transform(X_valid)

svc = svm.SVC(kernel='rbf',C=3, gamma=0.02)
svc.fit(X_train, y_train)
acc = svc.score(X_test,y_test)
acc_valid = svc.score(X_valid, y_valid)
print('PCA-SVM Test Score: ',acc)
print('PCA-SVM Valid Score: ', acc_valid)
